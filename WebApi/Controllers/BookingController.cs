﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
          public class BookingController : ApiController
          {
                    public IEnumerable<Rezervacije> Get()
                    {
                              using (BookingEntities entities = new BookingEntities())
                              {
                                        return entities.Rezervacijes.ToList();


                              }

                    }
                    //[Route("api/booking/send")]   
                    //[HttpGet]
                    //public IEnumerable<Objekt> Send()
                    //{
                    //          using (BookingEntities entities = new BookingEntities())
                    //          {
                    //                    return entities.Objekts.ToList();


                    //          }
                    //}

                    //[Route("api/booking/post")]
                    //[HttpPost]
                    public HttpResponseMessage Post([FromBody] Rezervacije rezervacija)
                    {
                              try
                              {
                                        using (BookingEntities entities = new BookingEntities())
                                        {
                                                  entities.Rezervacijes.Add(rezervacija);
                                                  entities.SaveChanges();
                                                  var message= Request.CreateResponse(HttpStatusCode.OK);
                                                  return message;
                                                  

                                        }
                              }
                              catch (Exception e)
                              {
                                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
                              }
                                                          

                    }
                    [Route("api/booking/update")]
                    [HttpPost]
                    public void Update([FromBody] Rezervacije rezervacija)
                    {
                              using (BookingEntities entities = new BookingEntities())
                              {
                                        Rezervacije r = entities.Rezervacijes.Where(p => p.Id == rezervacija.Id).FirstOrDefault();
                                        r.Gost = rezervacija.Gost;
                                        r.ImeObjekta = rezervacija.ImeObjekta;
                                        r.BrojSobe = rezervacija.BrojSobe;
                                        r.DatumDolaska = rezervacija.DatumDolaska;
                                        r.DatumOdlaska = rezervacija.DatumOdlaska;
                                        entities.SaveChanges();
                                        

                              }

                    }
                    [Route("api/booking/delete")]
                    [HttpPost]
                    public void Delete([FromBody] int id)
                    {
                              using (BookingEntities entities = new BookingEntities())
                              {

                                        Rezervacije r = entities.Rezervacijes.Where(p => p.Id == id).FirstOrDefault();
                                        entities.Rezervacijes.Remove(r);
                                        entities.SaveChanges();

                              }
                    }
          }
}
